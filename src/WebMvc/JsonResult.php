<?php
/**
 *
 */

namespace App\WebMvc;


class JsonResult implements \JsonSerializable
{
    /**
     * @var int
     */
    private $code;

    /**
     * @var string
     */
    private $msg;

    /**
     * @var null
     */
    private $data;

    /**
     * JsonResult constructor.
     * @param $code
     * @param $msg
     * @param $data
     */
    public function __construct(int $code, string $msg, $data = null)
    {
        $this->code = $code;
        $this->msg = $msg;
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function jsonSerialize() : array
    {
        return [
            'code' => $this->code,
            'msg'  => $this->msg,
            'data' => $this->data ?? null
        ];
    }
}