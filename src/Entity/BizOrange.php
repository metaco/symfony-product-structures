<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BizOrangeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class BizOrange
{
    use MetaDataTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $prototype;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $factoryData;

    /**
     * @ORM\Column(name="reset_able" ,type="boolean")
     */
    private $resetAble;


    public function getId()
    {
        return $this->id;
    }

    public function getPrototype(): ?string
    {
        return $this->prototype;
    }

    public function setPrototype(string $prototype): self
    {
        $this->prototype = $prototype;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getFactoryData(): ?string
    {
        return $this->factoryData;
    }

    public function setFactoryData(string $factoryData): self
    {
        $this->factoryData = $factoryData;

        return $this;
    }

    public function getResetAble(): ?bool
    {
        return $this->resetAble;
    }

    public function setResetAble(bool $resetAble): self
    {
        $this->resetAble = $resetAble;

        return $this;
    }
}
