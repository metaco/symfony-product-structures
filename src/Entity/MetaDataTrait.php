<?php
/**
 *
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


trait MetaDataTrait
{
    /**
     * @ORM\Column(name="meta_created", type="datetime")
     */
    private $metaCreated;

    /**
     * @ORM\Column(name="meta_updated", type="datetime")
     */
    private $metaUpdated;

    /**
     * @ORM\Column(name="meta_store_flag", type="smallint")
     */
    private $metaStoreFlag;


    public function getMetaCreated(): ?\DateTimeInterface
    {
        return $this->metaCreated;
    }

    /**
     * @ORM\PrePersist
     */
    public function setMetaCreated(): self
    {
        $this->metaCreated = new \DateTime('now');

        return $this;
    }

    public function getMetaUpdated(): ?\DateTimeInterface
    {
        return $this->metaUpdated;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setMetaUpdated(): self
    {
        $this->metaUpdated = new \DateTime('now');

        return $this;
    }


    public function getMetaStoreFlag(): ?int
    {
        return $this->metaStoreFlag;
    }

    /**
     * @ORM\PrePersist
     */
    public function setMetaStoreFlag(): self
    {
        $this->metaStoreFlag = 0;

        return $this;
    }
}