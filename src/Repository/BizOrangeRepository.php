<?php

namespace App\Repository;

use App\Entity\BizOrange;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BizOrange|null find($id, $lockMode = null, $lockVersion = null)
 * @method BizOrange|null findOneBy(array $criteria, array $orderBy = null)
 * @method BizOrange[]    findAll()
 * @method BizOrange[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BizOrangeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BizOrange::class);
    }

    /**
     * @param string $price
     * @return BizOrange[]
     */
    public function findByPrice(string $price)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.price = :price')
            ->setParameter('price', $price)
            ->getQuery()
            ->getResult();
    }

//    /**
//     * @return BizOrange[] Returns an array of BizOrange objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BizOrange
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
