<?php
/**
 *
 */

namespace App\Service\Traits;


use Psr\Log\LoggerInterface;

trait LoggerTrait
{
    /** @var LoggerInterface */
    protected $logger;

    /**
     * @required
     * @param mixed $logger
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }


    protected function info(string $message, $context = []) : void
    {
        $this->logger->info(
            sprintf("%s | -> %s | -> %s", get_class($this), __FUNCTION__, $message),
            $context
        );
    }

    protected function error(string $message, $context = [])
    {
        $this->logger->error(
            sprintf("%s | -> %s | -> %s", get_class($this), __FUNCTION__, $message),
            $context
        );
    }
}