<?php
/**
 *
 */

namespace App\Service\Database;


use App\Service\Traits\LoggerTrait as DatabaseLogger;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

abstract class DatabaseService
{
    use DatabaseLogger;

    protected $entityManager;

    /** @var EntityRepository */
    protected $entityRepository;

    /**
     * DatabaseService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $id
     * @param null $lockModel
     * @param null $lockVersion
     * @return null|object
     */
    public function get(int $id, $lockModel = null, $lockVersion = null) : ?object
    {
        return $this->entityRepository->find($id, $lockModel, $lockVersion);
    }

    /**
     * @param object $entity
     * @return int
     */
    public function save(object $entity) : int
    {
        $result = 0;

        try {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        } catch (\Throwable $e) {
            $this->error($e->getMessage());
            $result = -1;
        }

        return $result;
    }

    /**
     * @param object $entity
     */
    public function delete(object $entity) :void
    {
        $this->entityManager->remove($entity);
    }
}