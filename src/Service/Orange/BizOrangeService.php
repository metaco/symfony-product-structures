<?php
/**
 *
 */

namespace App\Service\Orange;


use App\Entity\BizOrange;
use App\Repository\BizOrangeRepository;
use App\Service\Database\DatabaseService;
use Doctrine\ORM\EntityManagerInterface;

class BizOrangeService extends DatabaseService
{
    /**
     * BizOrangeService constructor.
     * @param EntityManagerInterface $entityManager
     * @param BizOrangeRepository $bizOrangeRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        BizOrangeRepository $bizOrangeRepository
    )
    {
        parent::__construct($entityManager);

        $this->entityRepository = $bizOrangeRepository;
    }

    /**
     * @param string $price
     * @return \App\Entity\BizOrange[]
     */
    public function getOrangeBySpecificPrice(string $price)
    {
        $this->info("price:" .  $price);

        $bizOrange = $this->entityRepository->findByPrice($price);

        return $bizOrange;
    }

    public function storeOrangeSpecific(string $price)
    {
        $this->info("price :" . $price);

        $bizOrange = new BizOrange();
        $bizOrange->setPrice($price);
        $bizOrange->setPrototype("SymfonyLeave");
        $bizOrange->setFactoryData("version3-1");
        $bizOrange->setResetAble(false);

        $storeResult = $this->save($bizOrange);

        if ($storeResult === 0) {
            return $bizOrange;
        }

        return -1;
    }

    public function modifyMetaData(int $orangeId)
    {
        $this->info('orangeId: '. $orangeId);

        /** @var BizOrange $orangeEntity */
        $orangeEntity = $this->get($orangeId);

        $orangeEntity->setPrototype('modify after grant...');

        $this->save($orangeEntity);

        return $orangeEntity;
    }
}