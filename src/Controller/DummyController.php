<?php

namespace App\Controller;

use App\Service\Orange\BizOrangeService;
use App\Service\Traits\LoggerTrait;
use App\WebMvc\JsonResult;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Class DummyController
 * @package App\Controller
 * @Route("/biz")
 */
class DummyController extends Controller
{
    use LoggerTrait;

    private $bizOrangeService;

    /**
     * DummyController constructor.
     * @param $bizOrangeService
     */
    public function __construct(BizOrangeService $bizOrangeService)
    {
        $this->bizOrangeService = $bizOrangeService;
    }


    /**
     * @param $request
     * @Route("/oranges", name="biz_orange")
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $bizProduct = $this->bizOrangeService->getOrangeBySpecificPrice($request->get('price'));

        return $this->json(new JsonResult(0, "....", $bizProduct));
    }

    /**
     * @Route("/store")
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $this->logger->info('store :' . json_encode($request->request->all()));

        $price = $request->get('price');

        $result = $this->bizOrangeService->storeOrangeSpecific($price);

        return $this->json(new JsonResult(0, "have", $result));
    }

    /**
     * @Route("/modify")
     * @param Request $request
     * @return JsonResponse
     */
    public function modify(Request $request)
    {
        $orange = $this->bizOrangeService->modifyMetaData($request->get('orangeId'));

        if ($orange === null) {
            return $this->json(['code' => -1, 'result' => $orange]);
        }

        return $this->json(['code' => 2, 'result' => $orange->getId()]);
    }
}
